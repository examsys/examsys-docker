var dbPass = "password"
var clusterName = "devCluster"

try {
  print('Configuring instances to the cluster.\n');
  dba.configureInstance({user: "root", host: "mysql-server-1", password: dbPass})
  dba.configureInstance({user: "root", host: "mysql-server-2", password: dbPass})
  dba.configureInstance({user: "root", host: "mysql-server-3", password: dbPass})
  print('Setting up InnoDB cluster...\n');
  shell.connect('root@mysql-server-1:3306', dbPass)
  var cluster = dba.createCluster(clusterName);
  print('Adding instances to the cluster.\n');
  cluster.addInstance({user: "root", host: "mysql-server-2", password: dbPass})
  print('.');
  cluster.addInstance({user: "root", host: "mysql-server-3", password: dbPass})
  print('.\nInstances successfully added to the cluster.');
  print('\nInnoDB cluster deployed successfully.\n');
} catch(e) {
  print('\nThe InnoDB cluster could not be created.\n\nError: ' + e.message + '\n');
  try {
    dba.rebootClusterFromCompleteOutage(clusterName);
  } catch(e) {
    print('\nThe InnoDB cluster could not be rebooted.\n\nError: ' + e.message + '\n');
    print('\nYou may need to wait until mysql-server-1 logs "[Repl] Plugin group_replication reported: \'Setting super_read_only=ON.\'" before tryint to ru nthis script again.\n');
  }
}
